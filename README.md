# Simulador de Gerência de Memória Virtual

Guia rápido
-----------

Para compilar o código e executar o programa, basta executar os passos abaixo.

    % make
    % ./bin/simulador-memvirt


Características
---------------

Todos os processos têm o **mesmo tamanho** nesse simulador.

É possível alterar alguns dados de entrada do simulador. Todos esse valores podem
ser encontrados no arquivo `include/virtual_memory.h`. As constantes `FRAMES`
(em _bytes_), `NUMBER_OF_PROCESSES`, `PROCESS_SIZE`(em _bytes_), `WORKING_SET`.


Relatório
=========

Descrição
----------

O simulador de gerência de memória virtual com paginação, munido do algoritmo de
realocação relógio(aproximação do NRU). Toda simulação se passa através do uso de
_thread_. A primeira _thread_ é sempre a gerenciadora de memória virtual. É ela que
efetua o _swap in/out_ das páginas. O restante das _threads_, processos no nosso
contexto, são lançadas simultaneamente e se executam chamando sempre a página subsequente.

Inicialmente, a memória virtual é inicializada com o número negativo de um. isso
indica que ela está completamente vazia e nenhuma página foi alocada outrora.

Quando um processo faz uma referência a uma página, ele ativa uma mecanismo de
comunicação com o genrenciador da memória virtual. Através deste, ele consegue submeter
seu identificador (PID) e a página que está fazendo referência.

Assim que receber o sinal de sincronização de algum processo, o gerenciador verifica se
a página está na memória virtual(na nossa simulação), isto também indica que a mesma estaria 
na memória principal.

Caso a página não seja encontrada na memória virtual, ela ativa o mecanismo para
inserir a nova página. Se houver espaço suficiente, é inserida diretamente na
primeira posição livre.

Se o espaço for insuficiente, o gerenciador de memória virtual inicia o algoritmo
de realocação, conhecido como Algoritmo do Relógio. Assim que identificada a página 
a ser removida o simulador alerta que tal página será excluída da memória virtual.
Ao passo que a nova página é colocada em sua posição atual, como indicado o
ponteiro do relógio.

O simulador exibe  constantemente informações sobre o estado da tabela de páginas.
O modelo exibição é composto por PID do processo, sua tabela de
páginas correspondente, assim como a indicação do sinal de referência da página
(***** representa sinal de referência igual a um).

Simulação
---------

#### Simulação 1

Primeiramente, para efeito de exemplo, vou utilizar o programa supondo um mundo
perfeito. No nosso contexto, esse tal mundo perfeito seria um programa onde suas
página são chamadas contiguamente. Claramente, isso é apenas uma simulação.


    [PID 0] [ -1 -1 -1 -1]
    [MEM VIRT] [PID 0] Swap in página 0.
    [PID 0] [ 0 -1 -1 -1]
    [PID 0] [ 0 -1 -1 -1]
    [MEM VIRT] [PID 0] Swap in página 1.
    [PID 0] [ 0 1 -1 -1]
    [PID 0] [ 0 1 -1 -1]
    [MEM VIRT] [PID 0] Swap in página 2.
    [PID 0] [ 0 1 2 -1]
    [PID 0] [ 0 1 2 -1]
    [MEM VIRT] [PID 0] Swap in página 3.
    [PID 0] [ 0 1 2 3]
    [PID 0] [ 0 1 2 3]
    [MEM VIRT] [PID 0] Swap out página 0.
    [MEM VIRT] [PID 0] Swap in página 4.
    [PID 0] [ 4*  1 2 3]
    [PID 0] [ 4*  1 2 3]
    [MEM VIRT] [PID 0] Swap out página 1.
    [MEM VIRT] [PID 0] Swap in página 5.
    [PID 0] [ 4*  5*  2 3]
    [PID 0] [ 4*  5*  2 3]
    [MEM VIRT] [PID 0] Swap out página 2.
    [MEM VIRT] [PID 0] Swap in página 6.
    [PID 0] [ 4*  5*  6*  3]
    [PID 0] [ 4*  5*  6*  3]
    [MEM VIRT] [PID 0] Swap out página 3.
    [MEM VIRT] [PID 0] Swap in página 7.
    [PID 0] [ 4*  5*  6*  7* ]
    [PID 0] [ 4*  5*  6*  7* ]
    [MEM VIRT] [PID 0] Swap out página 4.
    [MEM VIRT] [PID 0] Swap in página 8.
    [PID 0] [ 8*  5 6 7]
    [PID 0] [ 8*  5 6 7]
    [MEM VIRT] [PID 0] Swap out página 5.
    [MEM VIRT] [PID 0] Swap in página 9.
    [PID 0] [ 8*  9*  6 7]
    [PID 0] [ 8*  9*  6 7]
    [MEM VIRT] [PID 0] Swap out página 6.
    [MEM VIRT] [PID 0] Swap in página 10.
    [PID 0] [ 8*  9*  10*  7]
    [PID 0] [ 8*  9*  10*  7]
    [MEM VIRT] [PID 0] Swap out página 7.
    [MEM VIRT] [PID 0] Swap in página 11.
    [PID 0] [ 8*  9*  10*  11* ]
    [PID 0] [ 8*  9*  10*  11* ]
    [MEM VIRT] [PID 0] Swap out página 8.
    [MEM VIRT] [PID 0] Swap in página 12.
    [PID 0] [ 12*  9 10 11]
    [PID 0] [ 12*  9 10 11]
    [MEM VIRT] [PID 0] Swap out página 9.
    [MEM VIRT] [PID 0] Swap in página 13.
    [PID 0] [ 12*  13*  10 11]
    [PID 0] [ 12*  13*  10 11]
    [MEM VIRT] [PID 0] Swap out página 10.
    [MEM VIRT] [PID 0] Swap in página 14.
    [PID 0] [ 12*  13*  14*  11]
    [PID 0] [ 12*  13*  14*  11]
    [MEM VIRT] [PID 0] Swap out página 11.
    [MEM VIRT] [PID 0] Swap in página 15.
    [PID 0] [ 12*  13*  14*  15* ]

Podemos perceber que quando as páginas são referenciadas contiguamente, o algoritmo
do relógio se comporta como o algoritmo LRU.

#### Simulação 2

Outra simulação feita agora com referência a páginas aleatórias pode ser observada abaixo.

    [PID 0] [ -1 -1 -1 -1]
    [MEM VIRT] [PID 0] Swap in página 14.
    [PID 0] [ 14 -1 -1 -1]
    [PID 0] [ 14 -1 -1 -1]
    [MEM VIRT] [PID 0] Swap in página 7.
    [PID 0] [ 14 7 -1 -1]
    [PID 0] [ 14 7 -1 -1]
    [MEM VIRT] [PID 0] Swap in página 10.
    [PID 0] [ 14 7 10 -1]
    [PID 0] [ 14 7 10 -1]
    [MEM VIRT] [PID 0] Swap in página 15.
    [PID 0] [ 14 7 10 15]
    [PID 0] [ 14 7 10 15]
    [MEM VIRT] [PID 0] Swap out página 14.
    [MEM VIRT] [PID 0] Swap in página 8.
    [PID 0] [ 8*  7 10 15]
    [PID 0] [ 8*  7 10 15]
    [MEM VIRT] [PID 0] Swap out página 7.
    [MEM VIRT] [PID 0] Swap in página 11.
    [PID 0] [ 8*  11*  10 15]
    [PID 0] [ 8*  11*  10 15]
    [MEM VIRT] [PID 0] Swap out página 10.
    [MEM VIRT] [PID 0] Swap in página 6.
    [PID 0] [ 8*  11*  6*  15]
    [PID 0] [ 8*  11*  6*  15]
    [MEM VIRT] [PID 0] Swap out página 15.
    [MEM VIRT] [PID 0] Swap in página 12.
    [PID 0] [ 8*  11*  6*  12* ]
    [PID 0] [ 8*  11*  6*  12* ]
    [MEM VIRT] [PID 0] Swap out página 8.
    [MEM VIRT] [PID 0] Swap in página 14.
    [PID 0] [ 14*  11 6 12]
    [PID 0] [ 14*  11 6 12]
    [MEM VIRT] [PID 0] Swap out página 11.
    [MEM VIRT] [PID 0] Swap in página 3.
    [PID 0] [ 14*  3*  6 12]
    [PID 0] [ 14*  3*  6 12]
    [MEM VIRT] [PID 0] Swap out página 6.
    [MEM VIRT] [PID 0] Swap in página 9.
    [PID 0] [ 14*  3*  9*  12]
    [PID 0] [ 14*  3*  9*  12]
    [MEM VIRT] [PID 0] Página 14 está na memória principal.
    [PID 0] [ 14*  3*  9*  12]
    [PID 0] [ 14*  3*  9*  12]
    [MEM VIRT] [PID 0] Swap out página 12.
    [MEM VIRT] [PID 0] Swap in página 2.
    [PID 0] [ 14*  3*  9*  2* ]
    [PID 0] [ 14*  3*  9*  2* ]
    [MEM VIRT] [PID 0] Swap out página 14.
    [MEM VIRT] [PID 0] Swap in página 0.
    [PID 0] [ 0*  3 9 2]
    [PID 0] [ 0*  3 9 2]
    [MEM VIRT] [PID 0] Página 0 está na memória principal.
    [PID 0] [ 0*  3 9 2]
    [PID 0] [ 0*  3 9 2]
    [MEM VIRT] [PID 0] Swap out página 3.
    [MEM VIRT] [PID 0] Swap in página 13.
    [PID 0] [ 0*  13*  9 2]
    [PID 0] [ 0*  13*  9 2]
    [MEM VIRT] [PID 0] Swap out página 9.
    [MEM VIRT] [PID 0] Swap in página 8.
    [PID 0] [ 0*  13*  8*  2]
    [PID 0] [ 0*  13*  8*  2]
    [MEM VIRT] [PID 0] Página 13 está na memória principal.
    [PID 0] [ 0*  13*  8*  2]
    [PID 0] [ 0*  13*  8*  2]
    [MEM VIRT] [PID 0] Swap out página 2.
    [MEM VIRT] [PID 0] Swap in página 11.
    [PID 0] [ 0*  13*  8*  11* ]
    [PID 0] [ 0*  13*  8*  11* ]
    [MEM VIRT] [PID 0] Swap out página 0.

#### Simulação 3

Agora utilizando quatro processos e acesso a referências aleatórias e sleep de três segundos.

    [PID 0] [ -1 -1 -1 -1]
    [MEM VIRT] [PID 0] Swap in página 1.
    [PID 0] [ 1 -1 -1 -1]
    [PID 1] [ -1 -1 -1 -1]
    [MEM VIRT] [PID 1] Swap in página 1.
    [PID 1] [ 1 -1 -1 -1]
    [PID 2] [ -1 -1 -1 -1]
    [MEM VIRT] [PID 2] Swap in página 1.
    [PID 2] [ 1 -1 -1 -1]
    [PID 3] [ -1 -1 -1 -1]
    [MEM VIRT] [PID 3] Swap in página 1.
    [PID 3] [ 1 -1 -1 -1]
    [PID 0] [ 1 -1 -1 -1]
    [MEM VIRT] [PID 0] Swap in página 10.
    [PID 0] [ 1 10 -1 -1]
    [PID 3] [ 1 -1 -1 -1]
    [MEM VIRT] [PID 3] Swap in página 3.
    [PID 3] [ 1 3 -1 -1]
    [PID 2] [ 1 -1 -1 -1]
    [MEM VIRT] [PID 2] Swap in página 12.
    [PID 2] [ 1 12 -1 -1]
    [PID 1] [ 1 -1 -1 -1]
    [MEM VIRT] [PID 1] Swap in página 8.
    [PID 1] [ 1 8 -1 -1]
    [PID 1] [ 1 8 -1 -1]
    [MEM VIRT] [PID 1] Swap in página 7.
    [PID 1] [ 1 8 7 -1]
    [PID 2] [ 1 12 -1 -1]
    [MEM VIRT] [PID 2] Swap in página 3.
    [PID 2] [ 1 12 3 -1]
    [PID 3] [ 1 3 -1 -1]
    [MEM VIRT] [PID 3] Swap in página 15.
    [PID 3] [ 1 3 15 -1]
    [PID 0] [ 1 10 -1 -1]
    [MEM VIRT] [PID 0] Swap in página 13.
    [PID 0] [ 1 10 13 -1]
    [PID 1] [ 1 8 7 -1]
    [MEM VIRT] [PID 1] Swap in página 9.
    [PID 1] [ 1 8 7 9]
    [PID 0] [ 1 10 13 -1]
    [MEM VIRT] [PID 0] Página 10 está na memória principal.
    [PID 0] [ 1 10 13 -1]
    [PID 3] [ 1 3 15 -1]
    [MEM VIRT] [PID 3] Swap in página 8.
    [PID 3] [ 1 3 15 8]
    [PID 2] [ 1*  12 3 -1]
    [MEM VIRT] [PID 2] Página 3 está na memória principal.
    [PID 2] [ 1*  12 3 -1* ]
    [PID 1] [ 1 8 7 9]
    [MEM VIRT] [PID 1] Swap out página 1.
    [MEM VIRT] [PID 1] Swap in página 12.
    [PID 1] [ 12*  8 7 9]
    [PID 0] [ 1 10 13 -1]
    [MEM VIRT] [PID 0] Swap in página 0.
    [PID 0] [ 1 10 13 0]
    [PID 3] [ 1 3 15 8]
    [MEM VIRT] [PID 3] Swap out página 1.
    [MEM VIRT] [PID 3] Swap in página 10.
    [PID 3] [ 10*  3 15 8]
    [PID 2] [ 1*  12 3 -1* ]
    [MEM VIRT] [PID 2] Swap in página 11.
    [PID 2] [ 1*  12 3 11]
    [PID 1] [ 12*  8 7 9]
    [MEM VIRT] [PID 1] Swap out página 8.
    [MEM VIRT] [PID 1] Swap in página 6.
    [PID 1] [ 12*  6*  7 9]
    [PID 0] [ 1 10 13 0]
    [MEM VIRT] [PID 0] Swap out página 1.
    [MEM VIRT] [PID 0] Swap in página 2.
    [PID 0] [ 2*  10 13 0]
    [PID 3] [ 10*  3 15 8]
    [MEM VIRT] [PID 3] Swap out página 3.
    [MEM VIRT] [PID 3] Swap in página 0.
    [PID 3] [ 10*  0*  15 8]
    [PID 2] [ 1*  12 3 11]
    [MEM VIRT] [PID 2] Swap out página 12.
    [MEM VIRT] [PID 2] Swap in página 9.
    [PID 2] [ 1 9*  3 11]
    [PID 1] [ 12*  6*  7 9]
    [MEM VIRT] [PID 1] Página 12 está na memória principal.
    [PID 1] [ 12*  6*  7 9]
    [PID 0] [ 2*  10 13 0]
    [MEM VIRT] [PID 0] Swap out página 10.
    [MEM VIRT] [PID 0] Swap in página 11.
    [PID 0] [ 2*  11*  13 0]
    [PID 2] [ 1 9*  3 11]
    [MEM VIRT] [PID 2] Página 3 está na memória principal.
    [PID 2] [ 1 9*  3 11* ]
    [PID 3] [ 10*  0*  15*  8]
    [MEM VIRT] [PID 3] Swap out página 8.
    [MEM VIRT] [PID 3] Swap in página 1.
    [PID 3] [ 10*  0*  15 1* ]
