CC := gcc
CFLAGS := -O4 -m64 -Wall
CLIBS = -lpthread

BIN_DIR := bin
BUILD_DIR := build
SRC_DIR := src

all: dirs virtual_memory manager process
	$(CC) $(CFLAGS) $(CLIBS) -o $(BIN_DIR)/simulador-memvirt $(SRC_DIR)/core/main.c $(BUILD_DIR)/virtual_memory.o $(BUILD_DIR)/manager.o $(BUILD_DIR)/process.o

virtual_memory:
	$(CC) $(CFLAGS) -c -o $(BUILD_DIR)/virtual_memory.o $(SRC_DIR)/virtual_memory/virtual_memory.c

manager:
	$(CC) $(CFLAGS) -c -o $(BUILD_DIR)/manager.o $(SRC_DIR)/virtual_memory/manager.c

process:
	$(CC) $(CFLAGS) -c -o $(BUILD_DIR)/process.o $(SRC_DIR)/virtual_memory/process.c

dirs:
	@mkdir -p $(BUILD_DIR) $(BIN_DIR)

clean_all: clean
	@rm -r $(BIN_DIR)

clean:
	@rm -r $(BUILD_DIR)
