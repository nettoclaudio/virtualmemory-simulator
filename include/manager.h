#ifndef _MANAGER_H_

    #define _MANAGER_H_

    extern void initialize_table_pages();

    extern void insert_page(int pid, int page);

    extern void show_table_page(int pid);

    extern int check_page_on_page_table(int pid, int page);

    extern void active_bit_reference(int pid, int page);

    extern void deactive_bit_reference(int pid, int page);

    extern void reallocate(int pid, int page);
#endif
