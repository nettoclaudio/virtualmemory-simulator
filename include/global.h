#ifndef _GLOBAL_H_
    #define _GLOBAL_H_

    #include <pthread.h>
    #include <semaphore.h>
    #include <stdio.h>
    #include <stdlib.h>

    #include "manager.h"
    #include "messages.h"
    #include "virtual_memory.h"

    #define FALSE 0
    #define TRUE  1

#endif
