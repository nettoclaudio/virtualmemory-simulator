#ifndef _MESSAGES_H_

    #define _MESSAGES_H_

    #define ERROR_INVALID_NUMBER_OF_PROCESSES "[ERRO] Quantidade inválida de processos.\n"
    #define ERROR_MEMORY_ALLOCATION "[ERRO] Não foi possível alocar memória.\n"
    #define ERROR_PTHREAD_CREATE "[ERRO] Não foi possível criar a thread.\n"
    #define ERROR_PTHREAD_JOIN "[ERRO] Erro ao esperar pelo término das threads.\n"

    #define INFO_PAGE_HIT "[MEM VIRT] [PID %d] Página %d está na memória principal.\n"

    #define INFO_SWAP_IN_PAGE "[MEM VIRT] [PID %d] Swap in página %d.\n"

    #define INFO_SWAP_OUT_PAGE "[MEM VIRT] [PID %d] Swap out página %d.\n"

#endif
