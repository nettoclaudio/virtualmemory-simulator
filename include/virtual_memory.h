#ifndef _VIRTUAL_MEMORY_H_

    #define _VIRTUAL_MEMORY_H_

    #define FRAMES 64
    #define WORKING_SET 4

    #define NUMBER_OF_PROCESSES 20
    #define PROCESS_SIZE 1024

    typedef struct _LINE_T_
    {
        unsigned frame_number;
        unsigned short reference;
    } line_t;

    typedef struct _TABLE_PAGE_T_
    {
        line_t   lines[WORKING_SET];
        unsigned pages;
        unsigned pointer;
    } table_page_t;

    sem_t mutex;

    sem_t send;

    sem_t receive;

    extern void* manager(void* args);

    extern void* process(void* args);

    extern void refer(int process_id, int page);

    extern void activate_bit_reference(int pid, int page);

#endif
