#include "../../include/global.h"

void* process(void* args)
{
    int page;
    int number_of_pages;

    int process_id = *(int*) args;
    free(args);

    number_of_pages = (int) (PROCESS_SIZE / FRAMES);

    for(page=0; page<number_of_pages; page++)
    {
        refer(process_id-1, page);
    }

    pthread_exit(NULL);
}
