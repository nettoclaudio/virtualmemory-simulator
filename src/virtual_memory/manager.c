#include "../../include/global.h"

table_page_t table_page[NUMBER_OF_PROCESSES];

void activate_bit_reference(int pid, int page)
{
    int i;

    for(i=0; i<WORKING_SET; i++)
    {
        if( table_page[pid].lines[i].frame_number == page)
        {
            table_page[pid].lines[i].reference = TRUE;
            break;
        }
    }
}

void deactivate_bit_reference(int pid, int page)
{
    int i;

    for(i=0; i<WORKING_SET; i++)
    {
        if( table_page[pid].lines[i].frame_number == page)
        {
            table_page[pid].lines[i].reference = FALSE;
            break;
        }
    }
}

void initialize_table_pages()
{
    int i;
    int j;

    for(i=0; i<NUMBER_OF_PROCESSES; i++)
    {
        table_page[i].pages = 0;
        table_page[i].pointer = 0;

        for(j=0; j<WORKING_SET; j++)
        {
            table_page[i].lines[j].frame_number = -1;
            table_page[i].lines[j].reference = -1;
        }
    }
}

int check_page_on_page_table(int pid, int page)
{
    int i;

    for(i=0; i<WORKING_SET; i++)
    {
        if( table_page[pid].lines[i].frame_number == page )
        {
            return TRUE;
        }
    }

    return FALSE;
}

void insert_page(int pid, int page)
{
    int i;

    if( table_page[pid].pages == WORKING_SET )
    {
        reallocate(pid, page);
        return;
    }

    for(i=0; i<WORKING_SET; i++)
    {
        if( table_page[pid].lines[i].frame_number == -1 )
        {
            table_page[pid].pages++;
            table_page[pid].lines[i].frame_number = page;
            table_page[pid].lines[i].reference = 0;

            fprintf(stdout, INFO_SWAP_IN_PAGE, pid, page);
            break;
        }
    }
}

void reallocate(int pid, int page)
{
    unsigned pointer = table_page[pid].pointer;
    short end = FALSE;

    while( end == FALSE )
    {
        if( table_page[pid].lines[pointer].reference == TRUE )
        {
            table_page[pid].lines[pointer].reference = 0;
        }
        else
        {
            fprintf(stdout, INFO_SWAP_OUT_PAGE, pid, table_page[pid].lines[pointer].frame_number);

            table_page[pid].lines[pointer].frame_number = page;
            table_page[pid].lines[pointer].reference = 0;

            fprintf(stdout, INFO_SWAP_IN_PAGE, pid, page);
            end = TRUE;
        }

        pointer++;
        pointer %= WORKING_SET;
    }

    table_page[pid].pointer = pointer;
}

void show_table_page(int pid)
{
    int i;

    fprintf(stdout, "[PID %d] [", pid);

    for(i=0; i<WORKING_SET; i++)
    {
        fprintf(stdout, " %d", table_page[pid].lines[i].frame_number);

        if( table_page[pid].lines[i].reference == TRUE )
        {
            fprintf(stdout, "* ");
        }
    }

    fprintf(stdout, "]\n");
}
