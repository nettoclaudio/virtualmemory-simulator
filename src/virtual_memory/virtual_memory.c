#include "../../include/global.h"

int next_pid;

int next_page;

void* manager(void* args)
{
    initialize_table_pages();

    while( TRUE )
    {
        sem_wait(&send);

        show_table_page(next_pid);

        if( check_page_on_page_table(next_pid, next_page) == FALSE )
        {
            insert_page(next_pid, next_page);
        }
        else
        {
            activate_bit_reference(next_pid, next_page);
            fprintf(stdout, INFO_PAGE_HIT, next_pid, next_page);
        }

        show_table_page(next_pid);

        sem_post(&receive);
    }

    pthread_exit(NULL);
}

void refer(int process_id, int page)
{
    sem_wait(&mutex);

    next_pid = process_id;
    next_page = page;

    sem_post(&send);
    sem_wait(&receive);

    sem_post(&mutex);
}
