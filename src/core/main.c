#include "../../include/global.h"

int main(int argc, char** argv)
{
    pthread_t* threads;

    int i;
    int number_of_threads;

    int* thread_id;

    sem_init(&mutex, 0, 1);
    sem_init(&send, 0, 0);
    sem_init(&receive, 0, 0);

    number_of_threads = NUMBER_OF_PROCESSES + 1;

    threads = (pthread_t*) malloc(sizeof(pthread_t)*number_of_threads);

    if( threads == NULL )
    {
        fprintf(stderr, ERROR_MEMORY_ALLOCATION);
        return EXIT_FAILURE;
    }

    if( pthread_create(threads, NULL, manager, NULL) )
    {
        fprintf(stderr, ERROR_PTHREAD_CREATE);
        return EXIT_FAILURE;
    }

    for(i=1; i<number_of_threads; i++)
    {
        thread_id = (int*) malloc(sizeof(int));

        if( thread_id == NULL )
        {
            fprintf(stderr, ERROR_MEMORY_ALLOCATION);
            return EXIT_FAILURE;
        }

        *thread_id = i;

        if( pthread_create(threads+i, NULL, process, (void*)thread_id) )
        {
            fprintf(stderr, ERROR_PTHREAD_CREATE);
            return EXIT_FAILURE;
        }
    }

    for(i=0; i<number_of_threads; i++)
    {
        if( pthread_join(*(threads+i), NULL) )
        {
            fprintf(stderr, ERROR_PTHREAD_JOIN);
            return EXIT_FAILURE;
        }
    }

    free(threads);

    return EXIT_SUCCESS;
}
